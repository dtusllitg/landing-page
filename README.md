# Landing page

This repository hosts my static landing page (old version available [here](https://gitlab.com/fhuitelec/old-landing-page)) powered by Gitlab CI and available at [about.huitelec.fr](https://about.huitelec.fr).